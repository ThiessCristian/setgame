#include "Game.h"
#include <set>
#include "Utils.h"


Game::Game(const size_t& nrOfPlayers) :m_deck(),m_dealer()
{
   m_players.resize(nrOfPlayers);
   m_players.push_back(Player());
   m_deck.shuffleDeck();
}

void Game::start()
{
   // the dealer deals 12 cards
   m_dealer.dealCards(&m_deck, &m_gameBoard);

   do {
      printGameInfo(m_players[0]);
      // if there are no sets on the board the dealer deals 3 more
      while (m_gameBoard.findSets() == false) {
         m_dealer.dealCards(&m_deck, &m_gameBoard, cardDealt);
      }
      m_gameBoard.printBoard();

      // we get a set with 3 indexes
      std::set<int> choices = m_players[0].getPlayerChoice();

      // check if its a set, if it is remove the cards and deal 3 more
      if (m_dealer.validateSet(&m_gameBoard, choices)) {
         validChoice(choices);
      }
      system("cls");
   } while (!m_deck.isEmpty());
 
   std::cout << "no more cards, you win";
}

void Game::printGameInfo(const Player& player) const
{
   std::cout << "deck size:" << m_deck.size() << std::endl;
   std::cout << "board size:" << m_gameBoard.size() << std::endl;
   std::cout << "player score:" << player.getScore() << std::endl;
}

void Game::validChoice(std::set<int> choices)
{
   // player gets a point for each set
   m_players[0].addPoint();
   m_dealer.removeSet(&m_gameBoard, choices);
   m_dealer.dealCards(&m_deck, &m_gameBoard, cardDealt);
}
