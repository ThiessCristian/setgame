#pragma once
#include <set>

class Player
{
   size_t m_score=0;

   bool validIndex( const size_t& index,const std::set<int>& choiceIndex) const;

public:
   Player();
   ~Player() = default;

   std::set<int> getPlayerChoice() const;
   void addPoint();
   void resetScore();
   size_t getScore()const;

};

