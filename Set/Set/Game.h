#pragma once

#include "GameBoard.h"
#include "Dealer.h"
#include "Player.h"
#include "Deck.h"

#include <vector>



class Game
{
   Dealer m_dealer;
   std::vector<Player> m_players;
   GameBoard m_gameBoard;
   Deck m_deck;



public:
   Game(const size_t& nrOfPlayers=1);
   void start();
   void printGameInfo(const Player& player)const;
   void validChoice(std::set<int>choices);
   ~Game() = default;
};

