#pragma once
#include <string>

class HistoryLogger
{
public:
   HistoryLogger();
   ~HistoryLogger() = default;

   static void logEvent(std::string eventText, std::string filename = "log.txt");
};

