#include "Colour.h"
#include <sstream>
#include <time.h>

Colour::Colour(BaseColour colour)
{
   switch (colour) {
      case BaseColour::RED: {
         *this=Colour(255, 0, 0);
         break;
      }
      case BaseColour::GREEN: {
         *this = Colour(0, 255, 0);
         break;
      }
      case BaseColour::BLUE: {
         *this = Colour(0, 0, 255);
         break;
      }
      default: {
         *this = Colour();
         break;
      }
   }
}

Colour::Colour(const uint8_t& red, const uint8_t& green, const uint8_t& blue):m_red(red),m_green(green),m_blue(blue)
{
}

void Colour::random()
{
   srand((size_t)time(NULL));

   m_red = rand() % 256;
   m_green = rand() % 256;
   m_blue = rand() % 256;

}
std::ostream & operator<<(std::ostream & o, const Colour & colour)
{
   o << "(" << colour.m_red << "." << colour.m_green << "." << colour.m_blue << ")";
   return o;
}

bool Colour::operator==(const Colour & colour) const
{
   return m_red==colour.m_red && m_green==colour.m_green &&m_blue==colour.m_blue;
}

bool Colour::operator!=(const Colour & colour) const
{
   return *this == colour;
}

bool Colour::operator<(const Colour & colour) const
{
   return (m_red*0.5 + m_blue*0.3 + m_green*0.2) < (colour.m_red*0.5 + colour.m_blue*0.3 + colour.m_green*0.2);
}
