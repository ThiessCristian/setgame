#pragma once
#include <array>
#include <vector>
#include "Card.h"
#include "Utils.h"

class Deck
{
   
   std::vector<Card> m_cards;
   size_t m_cardPosition = 0;

public:
   Deck();
   ~Deck() = default;

   void shuffleDeck();
   void printCards(size_t nrOfCards=maxNumberOfCards) const;
   Card* dealCard();
   bool isEmpty() const;
   size_t size()const;

};

