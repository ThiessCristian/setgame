#include "Dealer.h"
#include "GameBoard.h"
#include "Deck.h"
#include "Card.h"


Dealer::Dealer()
{}


void Dealer::dealCards(Deck* deck, GameBoard* boardCards, const size_t & nrOfCards) const
{
   for (size_t i = 0; i < nrOfCards; i++) {
      boardCards->addCard(deck->dealCard());
   }
}


bool Dealer::validateSet(GameBoard* board,std::set<int> set) const
{
   Card* card1 = board->getCardAt(*std::next(set.begin(), 0));
   Card* card2 = board->getCardAt(*std::next(set.begin(), 1));
   Card* card3 = board->getCardAt(*std::next(set.begin(), 2));
   
   return Card::validCards(*card1, *card2, *card3);
}

void Dealer::removeSet(GameBoard * boardCards, std::set<int> set) const
{
   // reverse order so the vector doesn't get messed up when removing items (the set it ordered asc)
   for (auto i = set.rbegin(); i != set.rend(); ++i) {
      boardCards->removeCardAt(*i);
   }
}

