#include "GameBoard.h"
#include "HistoryLogger.h"
#include "Card.h"


size_t GameBoard::m_cardsOnBoard = 0;

GameBoard::GameBoard()
{
   
}


GameBoard::~GameBoard()
{
   for (size_t i = 0; i < m_boardCards.size(); i++) {
      delete[] m_boardCards[i];
   }
   m_boardCards.clear();
}

size_t GameBoard::getCardsOnBoard()
{
   return m_cardsOnBoard;
}

void GameBoard::printBoard() const
{
   for (size_t i = 0; i < m_boardCards.size(); i++) {
      std::cout << i << *m_boardCards[i];
   }
}

void GameBoard::addCard(Card* card)
{
   m_cardsOnBoard++;
   m_boardCards.push_back(card);
}

Card * GameBoard::getCardAt(const size_t & index) const
{
   return m_boardCards[index];
}

void GameBoard::removeCardAt(const size_t & index)
{
   m_cardsOnBoard--;
   m_boardCards.erase(m_boardCards.begin() + index);
}

bool GameBoard::findSets() const
{
   // check if any combination of 3 cards from the board is a set
   for (size_t i = 0; i < m_boardCards.size()-2; i++) {
      for (size_t j = i+1; j < m_boardCards.size()-1; j++) {
         for (size_t k = j+1; k < m_boardCards.size(); k++) {
            if (Card::validCards(*m_boardCards[i], *m_boardCards[j], *m_boardCards[k])) {
               return true;
            }
         }
      }
   }
   return false;
}

size_t GameBoard::size() const
{
   return m_boardCards.size();
}


