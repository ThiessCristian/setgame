#include "Game.h"
#include <iostream>

#include "Card.h"

void main()
{
  // Game game;
  // game.start();

   Colour cl;
   cl.random();
   std::cout << cl;

   Card a(Number::ONE,Symbol::DIAMOND,Shading::OPEN,Colour(BaseColour::BLUE));
   Card b(Number::ONE, Symbol::OVAL, Shading::OPEN, Colour(BaseColour::GREEN));
   Card c(Number::ONE, Symbol::SQUIGGLE, Shading::OPEN, Colour(BaseColour::RED));

   std::cout << Card::validCards(a, b, c);


}