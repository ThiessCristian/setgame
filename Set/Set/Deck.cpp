#include "Deck.h"
#include <algorithm>
#include <time.h>
#include "Utils.h"

//generates each type of card in the deck
Deck::Deck()
{
   for (size_t i = 0; i < varietyNumber; i++) {
      Colour colour(static_cast<BaseColour>(i));

      for (size_t j = 0; j < varietyNumber; j++) {
         Shading shading = static_cast<Shading>(j);

         for (size_t k = 0; k < varietyNumber; k++) {
            Symbol symbol = static_cast<Symbol>(k);
            m_cards.push_back(Card(Number::ONE, symbol, shading, colour));
            m_cards.push_back(Card(Number::TWO, symbol, shading, colour));
            m_cards.push_back(Card(Number::THREE, symbol, shading, colour));
         }
      }
    }
}

void Deck::shuffleDeck()
{
   srand((size_t)time(NULL));
   std::random_shuffle(m_cards.begin(), m_cards.end());
}

void Deck::printCards(size_t numberOfCards) const
{
   for (size_t i = 0; i < numberOfCards; i++) {
      std::cout <<i<< m_cards[i];
   }
}

Card* Deck::dealCard()
{
   m_cardPosition++;
   return &m_cards[m_cardPosition - 1];
}

bool Deck::isEmpty() const
{
   return m_cardPosition == maxNumberOfCards;
}

size_t Deck::size() const
{
   return m_cards.size()-m_cardPosition;
}
