#pragma once

#include "Colour.h"
#include <iostream>
#include <map>

enum class Number
{
   ONE,
   TWO,
   THREE,
};

enum class Symbol
{
   DIAMOND,
   SQUIGGLE,
   OVAL
};

enum class Shading
{
   SOLID,
   STRIPED,
   OPEN
};



class Card
{
   Number m_number;
   Symbol m_symbol;
   Shading m_shading;
   Colour m_colour;
public:
   Card();
   Card(const Number& number, const Symbol& symbol, const Shading& shading, const Colour& colour);
   ~Card()=default;

   bool operator==(const Card& card) const;

   friend std::ostream& operator<<(std::ostream& o, const Card& card);

   static bool validCards(const Card& card1, const Card& card2, const Card& card3);

private:
   // this function is template because the card atributes differ by type and not all are enums, Colour is a class
   template <typename T>
   static bool allEqualOrNot(T atr1, T atr2, T atr3)
   {
      bool returnValue = false;
      if (atr1 == atr2 && atr2 == atr3) {
         returnValue = true;
      } else if (atr1 != atr2 && atr2 != atr3 && atr3 != atr1) {
         returnValue = true;
      }
      return returnValue;
   }
};

