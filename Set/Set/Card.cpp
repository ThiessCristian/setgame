#include "Card.h"
#include "Utils.h"


Card::Card()
{}

Card::Card(const Number & number, const Symbol & symbol, const Shading & shading, const Colour & colour):
   m_number(number),m_symbol(symbol),m_shading(shading),m_colour(colour)
{
}

bool Card::operator==(const Card & card) const
{
   return m_number == card.m_number && m_symbol == card.m_symbol && m_shading == card.m_shading && m_colour == card.m_colour;
}

bool Card::validCards(const Card & card1, const Card & card2, const Card & card3)
{

   bool numberCheck = allEqualOrNot(card1.m_number, card2.m_number, card3.m_number);
   bool symbolCheck = allEqualOrNot(card1.m_symbol, card2.m_symbol, card3.m_symbol);
   bool shadingCheck = allEqualOrNot(card1.m_symbol, card2.m_symbol, card3.m_symbol);
   bool colourCheck = allEqualOrNot(card1.m_symbol, card2.m_symbol, card3.m_symbol);

   return numberCheck && symbolCheck && shadingCheck && colourCheck;
}

std::ostream & operator<<(std::ostream & o, const Card & card)
{
   o << "[";
   o << numberStrings.at(card.m_number)<<" ";
   o << symbolStrings.at(card.m_symbol)<<" ";
   o << colourStrings.at(card.m_colour)<<" ";
   o << shadingStrings.at(card.m_shading)<<"]"<< std::endl;
   return o;
}
