#pragma once
#include <map>
#include "Colour.h"
#include "Card.h"


const size_t varietyNumber = 3;  //nr of types of atributes ex:colour, shading, number
const size_t maxNumberOfCards = 81;

const size_t cardDealt = 3;

const size_t chosenCards = 3;


//used to convert enums to strings
typedef const std::map<const Symbol, const std::string> SymbolStringMap;
typedef const std::map<const Shading, const std::string> ShadingStringMap;
typedef const std::map<const Colour, const std::string> ColourStringMap;
typedef const std::map<const Number, const std::string> NumberStringMap;


SymbolStringMap symbolStrings = {{Symbol::DIAMOND,"Diamond"},
                                 {Symbol::OVAL,"Oval"},
                                 {Symbol::SQUIGGLE,"Squiggle"}};

ShadingStringMap shadingStrings = {{Shading::OPEN,"Open"},
                                   {Shading::SOLID,"Solid"},
                                   {Shading::STRIPED,"Striped"}};

NumberStringMap numberStrings = {{Number::ONE,"One"},
                                 {Number::TWO,"Two"},
                                 {Number::THREE,"Three"}};

ColourStringMap colourStrings = {{Colour(BaseColour::RED),"Red"},
                                 {Colour(BaseColour::GREEN),"Green"},
                                 {Colour(BaseColour::BLUE),"Blue"}};