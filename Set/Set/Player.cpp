#include "Player.h"
#include <iostream>
#include "GameBoard.h"
#include "Utils.h"



Player::Player()
{}

bool Player::validIndex(const size_t& index,const std::set<int>& choiceIndex) const
{
   // the index should be less than the number of cards on the board and should not have been picked yet
   return index < GameBoard::getCardsOnBoard() && choiceIndex.find(index) == choiceIndex.end();
}


std::set<int> Player::getPlayerChoice() const
{
   std::set<int> choiceIndex;
   // the player chooses 3 cards
   for (size_t i = 0; i < chosenCards; i++) {
      bool wrongValue;
      do {
         wrongValue = true;
         std::cout << "index:";
         size_t index;
         std::cin >> index;

         if (validIndex(index,choiceIndex)) {
            choiceIndex.insert(index);
            wrongValue = false;
         } else {
            std::cout << "wrong index"<<std::endl;
         }
      } while (wrongValue);// we repeat the pick state if one cards was not good
   }
   return choiceIndex;
}

void Player::addPoint()
{
   m_score++;
}

void Player::resetScore()
{
   m_score = 0;
}

size_t Player::getScore() const
{
   return m_score;
}
