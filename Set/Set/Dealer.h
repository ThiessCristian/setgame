#pragma once
#include <set>

class Deck;
class GameBoard;


class Dealer
{
public:
   Dealer();
   ~Dealer() = default;
   void dealCards(Deck* deck, GameBoard* boardCards, const size_t & nrOfCards = 12) const;
   bool validateSet(GameBoard* board,std::set<int> set) const;
   void removeSet(GameBoard* boardCards, std::set<int> set) const;
};

