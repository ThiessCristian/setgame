#pragma once

#include <vector>
class Card;

class GameBoard
{
   std::vector<Card*> m_boardCards;
   static size_t m_cardsOnBoard;

public:
   GameBoard();
   ~GameBoard();

   static size_t getCardsOnBoard();

   void printBoard() const;
   void addCard(Card* card);
   Card* getCardAt(const size_t& index) const;
   void removeCardAt(const size_t& index);
   bool findSets() const;
   size_t size()const;

};

